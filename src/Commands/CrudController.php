<?php

namespace Adinal\LaravelCrud\Commands;

use Adinal\LaravelCrud\Crud;

class CrudController extends Crud
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crud:controller 
        {name : Class (singular) for example User}
        {--a|api : Controller for API resource}
        {--m|model= : Generate a resource controller for the given model}
    ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate controller scaffolding';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'Controller';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = $this->argument('name');
        $model = $this->option('model');
        $path = app_path("/Http/Controllers/{$name}Controller.php");

        if (file_exists($path)) {
            $this->error($this->type.' already exists!');

            return false;
        }

        if ($model) {
            if (preg_match('([^A-Za-z_/\\\\])', $model)) {
                $this->error('Model name contains invalid characters');
                return false;
            }
            
            $template = str_replace([
                    '{{Name}}',
                    '{{NameSingular}}'
                ],
                [
                    $name,
                    strtolower($name)
                ],
                $this->getStub($this->type.'/Model.stub')
            );

            file_put_contents($path, $template);

            $this->info($this->type." created successfully.");

        } else {
            $template = str_replace([
                '{{DummyName}}',
                '{{DummyNameLower}}'
            ],
            [
                $name,
                strtolower($name)
            ],
                $this->getStub('Controller/Base.stub')
            );
    
            file_put_contents($path, $template);
    
            $this->info("Controller created successfully.");
        }
    }
}
