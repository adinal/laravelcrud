<?php

namespace Adinal\LaravelCrud\Commands;

use Adinal\LaravelCrud\Crud;

class CrudInstall extends Crud
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crud:install 
        {name? : Class (singular) for example User}
        {--a|api : Controller for API resource}
        {--m|model= : Generate a resource controller for the given model}
    ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate controller scaffolding';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'Install';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->call('make:auth');
        $this->searchAndSort();
        $this->saveSearch();
        $this->layoutsApp();
    }

    public function searchAndSort() {
        $path = app_path("Http/Controllers/Controller.php");

        $stub = file_get_contents($path);

        if (preg_match('/searchAndSort/', $stub)) {
            $this->error('searchAndSort already exists');

            return false;
        } 

        $append = $this->getStub('Controller/SearchAndSort.stub');
        
        $template = str_replace("\n}", $append, $stub);

        file_put_contents($path, $template);

        $this->info('feature search and sort has been added.');
    }
    
    public function saveSearch() {
        $path = app_path("Http/Controllers/Controller.php");
        
        $stub = file_get_contents($path);

        if (preg_match('/saveSearchResult/', $stub)) {
            $this->error('saveSearchResult already exists');

            return false;
        } 

        $append = $this->getStub('Controller/SaveSearch.stub');
        
        $template = str_replace("\n}", $append, $stub);
        
        file_put_contents($path, $template);

        $this->info('feature save search has been added.');
    }

    public function layoutsApp() {
        $path = resource_path("views/layouts/app.blade.php");

        $template = $this->getStub('View/App.stub');

        file_put_contents($path, $template);
    }
}
