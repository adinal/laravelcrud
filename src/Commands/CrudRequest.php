<?php

namespace Adinal\LaravelCrud\Commands;

use Adinal\LaravelCrud\Crud;

class CrudRequest extends Crud
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crud:request 
        {name : Class (singular) for example User}
    ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate request scaffolding';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'Request';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = $this->argument('name');

        if(!file_exists($path = app_path('/Http/Requests'))) {
            mkdir($path, 0777, true);
        }

        $path = app_path("/Http/Requests/{$name}Request.php");

        $template = str_replace(
            ['{{DummyName}}'],
            [$name],
            $this->getStub('Request/Base.stub')
        );

        file_put_contents($path, $template);

        $this->info("Request created successfully.");
    }
}
