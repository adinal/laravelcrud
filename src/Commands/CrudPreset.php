<?php

namespace Adinal\LaravelCrud\Commands;

use Illuminate\Console\Command;
use Adinal\LaravelCrud\Crud;

class CrudPreset extends Crud
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crud:preset';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate default preset';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'Preset';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = $this->name;
        $type = $this->type;
        $path = $this->getPath($type);
        $this->info($path);
    }
}
