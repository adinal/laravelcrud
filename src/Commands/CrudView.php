<?php

namespace Adinal\LaravelCrud\Commands;

use Adinal\LaravelCrud\Crud;

class CrudView extends Crud
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crud:view
        {name : Class (singular) for example user}
    ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate controller scaffolding';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'View';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = $this->argument('name');
        $namelowercase = strtolower($name);

        $path = resource_path("/views/{$namelowercase}");

        if (file_exists($path)) {
            $this->error($this->type.' already exists!');

            return false;
        } else {
            mkdir($path, 0777, true);
        }

        $indexTemplate = str_replace([
                '{{DummyName}}',
                '{{DummyNameLower}}'
            ],
            [
                $name,
                strtolower($name)
            ],
            $this->getStub($this->type.'/Index.stub')
        );

        file_put_contents($path."/index.blade.php", $indexTemplate);
        
        $createTemplate = str_replace([
                '{{DummyName}}',
                '{{DummyNameLower}}'
            ],
            [
                $name,
                strtolower($name)
            ],
            $this->getStub($this->type.'/Create.stub')
        );

        file_put_contents($path."/create.blade.php", $createTemplate);
        
        $showTemplate = str_replace([
                '{{DummyName}}',
                '{{DummyNameLower}}'
            ],
            [
                $name,
                strtolower($name)
            ],
            $this->getStub($this->type.'/Show.stub')
        );

        file_put_contents($path."/show.blade.php", $showTemplate);
        
        $editTemplate = str_replace([
                '{{DummyName}}',
                '{{DummyNameLower}}'
            ],
            [
                $name,
                strtolower($name)
            ],
            $this->getStub($this->type.'/Edit.stub')
        );

        file_put_contents($path."/edit.blade.php", $editTemplate);

        $this->info($this->type." created successfully.");
    }
}
