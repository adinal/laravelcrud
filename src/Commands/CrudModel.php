<?php

namespace Adinal\LaravelCrud\Commands;

use Adinal\LaravelCrud\Crud;

class CrudModel extends Crud
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crud:model 
        {name : The name of class | for example [User] (must be singular)}
        {--a|all : Generate a migration, and resource controller for model}
        {--c|controller : Create a new resource controller for model}
        {--m|migration : Create a new migration for model}
        {--r|request : Create a new request for model}
        {--p|preset : Create preset scaffolding for model}
    ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate model scaffolding';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'Model';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Get Arguments
        $name = $this->argument('name');

        // Check if exist
        $path = app_path("/{$name}.php");

        if (file_exists($path)) {
            $this->error($this->type.' already exists!');

            return false;
        }

        // Get options
        $all = $this->option('all');
        $contoller = $this->option('controller');
        $migration = $this->option('migration');
        $request = $this->option('request');
        $preset = $this->option('preset');

        if ($all) {
            $this->model($name, $path);
            $this->migrations($name);
            $this->controller($name);
            $this->request($name);
            $this->preset($name);

            return false;
        }

        $this->model($name, $path);

        if ($migration) {
            $this->migrations($name);
        }
        
        if ($contoller) {
            $this->controller($name);
        }
        
        if ($request) {
            $this->request($name);
        }
        
        if ($preset) {
            $this->preset($name);
        }
    }

    public function model($name, $path) {
        $template = str_replace([
                '{{DummyName}}',
                '{{DummyNamesLower}}'
            ],
            [
                $name,
                strtolower(str_plural($name))
            ],
            $this->getStub($this->type.'/Base.stub')
        );

        file_put_contents($path, $template);

        $this->info("Model created successfully.");
    }

    public function migrations($name) {
        $table =  date('Y_m_d_His').'_create_'.strtolower(str_plural($name).'_table.php');

        $path = database_path("/migrations/{$table}");

        $template = str_replace([
                '{{DummyNames}}',
                '{{DummyNamesLower}}'
            ],
            [
                str_plural($name),
                strtolower(str_plural($name))
            ],
            $this->getStub($this->type.'/Migration.stub')
        );

        file_put_contents($path, $template);

        $this->info("Migrations created successfully.");
    }

    public function controller($name) {
        $path = app_path("/Http/Controllers/{$name}Controller.php");

        $template = str_replace([
            '{{DummyName}}',
            '{{DummyNameLower}}'
        ],
        [
            $name,
            strtolower($name)
        ],
            $this->getStub('Controller/Base.stub')
        );

        file_put_contents($path, $template);

        $this->info("Controller created successfully.");
    }
    
    public function request($name) {
        if(!file_exists($path = app_path('/Http/Requests'))) {
            mkdir($path, 0777, true);
        }

        $path = app_path("/Http/Requests/{$name}Request.php");

        $template = str_replace(
            ['{{DummyName}}'],
            [$name],
            $this->getStub('Request/Base.stub')
        );

        file_put_contents($path, $template);

        $this->info("Request created successfully.");
    }

    public function preset($name) {
        $this->call('crud:view', [
            'name' => $name
        ]);
    }
}
