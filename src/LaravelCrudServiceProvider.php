<?php

namespace Adinal\LaravelCrud;

use Illuminate\Support\ServiceProvider;
use Adinal\LaravelCrud\Commands\CrudPreset;
use Adinal\LaravelCrud\Commands\CrudController;
use Adinal\LaravelCrud\Commands\CrudView;
use Adinal\LaravelCrud\Commands\CrudModel;
use Adinal\LaravelCrud\Commands\CrudRequest;
use Adinal\LaravelCrud\Commands\CrudInstall;

class LaravelCrudServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->commands([
            CrudPreset::class,
            CrudController::class,
            CrudView::class,
            CrudModel::class,
            CrudRequest::class,
            CrudInstall::class,
        ]);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
