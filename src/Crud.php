<?php

namespace Adinal\LaravelCrud;

use Illuminate\Console\Command;

class Crud extends Command
{
    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub($type)
    {
        $path = base_path("vendor/adinal/laravelcrud/src/Stubs/{$type}");

        return file_get_contents($path);
    }

    /**
     * Get the destination class path.
     *
     * @param string $name
     *
     * @return string
     */
    protected function getPath($name)
    {
        return resource_path('.blade.php');
    }
}
